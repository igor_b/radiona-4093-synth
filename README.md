Radiona.org mini touch synth based on CD4093.

Inspired by https://andreassiagian.wordpress.com/2014/10/30/tutorial-simple-bio-synth-instrument/

More info (in Croatian): [https://radiona.org/wiki/project/radiona_touch_synth](https://radiona.org/wiki/project/radiona_touch_synth)
