EESchema Schematic File Version 4
LIBS:radiona4093-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L radiona4093-rescue:CD4093 U1
U 1 1 58B4304D
P 4900 3100
F 0 "U1" H 5300 3200 60  0000 C CNN
F 1 "CD4093" H 5250 2400 60  0000 C CNN
F 2 "Housings_DIP:DIP-14_W7.62mm_LongPads" H 4900 3100 60  0001 C CNN
F 3 "" H 4900 3100 60  0001 C CNN
	1    4900 3100
	0    1    1    0   
$EndComp
$Comp
L radiona4093-rescue:CONN_01X01 TOUCH1
U 1 1 58B430EF
P 4000 4750
F 0 "TOUCH1" V 4100 4750 50  0000 C CNN
F 1 "TOUCH1" V 4100 4750 50  0001 C CNN
F 2 "SmallPad:SmallPad" H 4000 4750 50  0001 C CNN
F 3 "" H 4000 4750 50  0000 C CNN
	1    4000 4750
	0    1    1    0   
$EndComp
$Comp
L radiona4093-rescue:CONN_01X01 TOUCH2
U 1 1 58B43138
P 4400 4750
F 0 "TOUCH2" V 4500 4750 50  0000 C CNN
F 1 "TOUCH2" V 4500 4750 50  0001 C CNN
F 2 "SmallPad:SmallPad" H 4400 4750 50  0001 C CNN
F 3 "" H 4400 4750 50  0000 C CNN
	1    4400 4750
	0    1    1    0   
$EndComp
$Comp
L radiona4093-rescue:CONN_01X01 TOUCH3
U 1 1 58B4315F
P 4750 4750
F 0 "TOUCH3" V 4850 4750 50  0000 C CNN
F 1 "TOUCH3" V 4850 4750 50  0001 C CNN
F 2 "SmallPad:SmallPad" H 4750 4750 50  0001 C CNN
F 3 "" H 4750 4750 50  0000 C CNN
	1    4750 4750
	0    1    1    0   
$EndComp
$Comp
L radiona4093-rescue:CONN_01X01 TOUCH4
U 1 1 58B4317E
P 5100 4750
F 0 "TOUCH4" V 5200 4750 50  0000 C CNN
F 1 "TOUCH4" V 5200 4750 50  0001 C CNN
F 2 "SmallPad:SmallPad" H 5100 4750 50  0001 C CNN
F 3 "" H 5100 4750 50  0000 C CNN
	1    5100 4750
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 4550 4000 4050
Wire Wire Line
	3750 4050 4000 4050
Wire Wire Line
	4300 4050 4300 3850
Wire Wire Line
	4400 4550 4400 4200
Wire Wire Line
	4400 4200 4500 4200
Wire Wire Line
	4500 4200 4500 3900
Wire Wire Line
	4750 4550 4750 4200
Wire Wire Line
	4750 4200 4600 4200
Wire Wire Line
	4600 4200 4600 3900
Wire Wire Line
	5100 4550 5100 4050
Wire Wire Line
	4800 4050 5100 4050
Wire Wire Line
	4800 4050 4800 3850
$Comp
L radiona4093-rescue:VDD #PWR02
U 1 1 58B432B1
P 5550 3700
F 0 "#PWR02" H 5550 3550 50  0001 C CNN
F 1 "VDD" H 5550 3850 50  0000 C CNN
F 2 "" H 5550 3700 50  0000 C CNN
F 3 "" H 5550 3700 50  0000 C CNN
	1    5550 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 3850 4900 3850
Wire Wire Line
	4700 3850 4700 3950
Wire Wire Line
	4400 3950 4700 3950
Wire Wire Line
	4900 3950 4900 3850
Wire Wire Line
	4400 3850 4400 3950
Connection ~ 4700 3950
$Comp
L radiona4093-rescue:C C1
U 1 1 58B43394
P 3750 4200
F 0 "C1" H 3775 4300 50  0000 L CNN
F 1 "100n" H 3775 4100 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W2.5mm_P5.00mm" H 3788 4050 50  0001 C CNN
F 3 "" H 3750 4200 50  0000 C CNN
	1    3750 4200
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:C C2
U 1 1 58B433C7
P 5300 4200
F 0 "C2" H 5325 4300 50  0000 L CNN
F 1 "1u" H 5325 4100 50  0000 L CNN
F 2 "Capacitors_THT:C_Rect_L7.0mm_W4.5mm_P5.00mm" H 5338 4050 50  0001 C CNN
F 3 "" H 5300 4200 50  0000 C CNN
	1    5300 4200
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:GND #PWR03
U 1 1 58B4347A
P 5300 4400
F 0 "#PWR03" H 5300 4150 50  0001 C CNN
F 1 "GND" H 5300 4250 50  0000 C CNN
F 2 "" H 5300 4400 50  0000 C CNN
F 3 "" H 5300 4400 50  0000 C CNN
	1    5300 4400
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:GND #PWR04
U 1 1 58B4349E
P 3750 4400
F 0 "#PWR04" H 3750 4150 50  0001 C CNN
F 1 "GND" H 3750 4250 50  0000 C CNN
F 2 "" H 3750 4400 50  0000 C CNN
F 3 "" H 3750 4400 50  0000 C CNN
	1    3750 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 4400 3750 4350
Wire Wire Line
	5300 4400 5300 4350
Connection ~ 4000 4050
Connection ~ 5100 4050
Wire Wire Line
	4400 3100 4400 3000
Wire Wire Line
	4400 3000 4500 3000
Wire Wire Line
	4500 3000 4500 3100
Wire Wire Line
	4800 3100 4800 3000
Wire Wire Line
	4750 3000 4800 3000
Wire Wire Line
	4900 3000 4900 3100
Wire Wire Line
	4600 3900 4650 3900
Wire Wire Line
	4650 3900 4650 3450
Wire Wire Line
	4650 3450 4750 3450
Wire Wire Line
	4750 3450 4750 3000
Connection ~ 4800 3000
Connection ~ 4600 3900
Wire Wire Line
	4500 3900 4550 3900
Wire Wire Line
	4550 3900 4550 3000
Connection ~ 4500 3000
Connection ~ 4500 3900
$Comp
L radiona4093-rescue:LED D1
U 1 1 58B43678
P 4750 2350
F 0 "D1" H 4750 2450 50  0000 C CNN
F 1 "LED" H 4750 2250 50  0001 C CNN
F 2 "LEDs:LED_D3.0mm" H 4750 2350 50  0001 C CNN
F 3 "" H 4750 2350 50  0000 C CNN
	1    4750 2350
	-1   0    0    1   
$EndComp
$Comp
L radiona4093-rescue:LED D2
U 1 1 58B436BF
P 4850 2550
F 0 "D2" H 4850 2650 50  0000 C CNN
F 1 "LED" H 4850 2450 50  0001 C CNN
F 2 "LEDs:LED_D3.0mm" H 4850 2550 50  0001 C CNN
F 3 "" H 4850 2550 50  0000 C CNN
	1    4850 2550
	-1   0    0    1   
$EndComp
Wire Wire Line
	4600 3100 4600 2350
Wire Wire Line
	4700 3100 4700 2550
$Comp
L radiona4093-rescue:GND #PWR05
U 1 1 58B4386E
P 5450 2700
F 0 "#PWR05" H 5450 2450 50  0001 C CNN
F 1 "GND" H 5450 2550 50  0000 C CNN
F 2 "" H 5450 2700 50  0000 C CNN
F 3 "" H 5450 2700 50  0000 C CNN
	1    5450 2700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5450 2700 5450 2550
Wire Wire Line
	5450 2550 5550 2550
Wire Wire Line
	4900 2350 5200 2350
Wire Wire Line
	5200 2350 5200 2450
Wire Wire Line
	5200 2450 5450 2450
Wire Wire Line
	5200 2550 5000 2550
Connection ~ 5200 2450
$Comp
L radiona4093-rescue:GND #PWR06
U 1 1 58B43951
P 4000 3150
F 0 "#PWR06" H 4000 2900 50  0001 C CNN
F 1 "GND" H 4000 3000 50  0000 C CNN
F 2 "" H 4000 3150 50  0000 C CNN
F 3 "" H 4000 3150 50  0000 C CNN
	1    4000 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 3150 4000 3000
Wire Wire Line
	4000 3000 4300 3000
Wire Wire Line
	4300 3000 4300 3100
$Comp
L radiona4093-rescue:PWR_FLAG #FLG07
U 1 1 58B43B0C
P 7000 3750
F 0 "#FLG07" H 7000 3845 50  0001 C CNN
F 1 "PWR_FLAG" H 7000 3930 50  0000 C CNN
F 2 "" H 7000 3750 50  0000 C CNN
F 3 "" H 7000 3750 50  0000 C CNN
	1    7000 3750
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:PWR_FLAG #FLG08
U 1 1 58B43C3A
P 6550 3750
F 0 "#FLG08" H 6550 3845 50  0001 C CNN
F 1 "PWR_FLAG" H 6550 3930 50  0000 C CNN
F 2 "" H 6550 3750 50  0000 C CNN
F 3 "" H 6550 3750 50  0000 C CNN
	1    6550 3750
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:GND #PWR09
U 1 1 58B43CA5
P 6550 3900
F 0 "#PWR09" H 6550 3650 50  0001 C CNN
F 1 "GND" H 6550 3750 50  0000 C CNN
F 2 "" H 6550 3900 50  0000 C CNN
F 3 "" H 6550 3900 50  0000 C CNN
	1    6550 3900
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:VDD #PWR010
U 1 1 58B43CCF
P 7300 3750
F 0 "#PWR010" H 7300 3600 50  0001 C CNN
F 1 "VDD" H 7300 3900 50  0000 C CNN
F 2 "" H 7300 3750 50  0000 C CNN
F 3 "" H 7300 3750 50  0000 C CNN
	1    7300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7300 3750 7000 3750
Wire Wire Line
	6550 3750 6550 3900
Wire Wire Line
	4700 3950 4900 3950
Wire Wire Line
	4000 4050 4300 4050
Wire Wire Line
	5100 4050 5300 4050
Wire Wire Line
	4800 3000 4900 3000
Wire Wire Line
	4600 3900 4600 3850
Wire Wire Line
	4500 3000 4550 3000
Wire Wire Line
	4500 3900 4500 3850
Wire Wire Line
	5200 2450 5200 2550
$Comp
L Connector:AudioJack3_SwitchTR J1
U 1 1 5D574E86
P 5750 2450
F 0 "J1" H 5470 2283 50  0000 R CNN
F 1 "AudioJack3_SwitchTR" H 5470 2374 50  0000 R CNN
F 2 "Jack_Stereo_3.5mm:Jack_Stereo_3.5mm" H 5750 2450 50  0001 C CNN
F 3 "~" H 5750 2450 50  0001 C CNN
	1    5750 2450
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 2250 5450 2250
Wire Wire Line
	5450 2250 5450 2450
Connection ~ 5450 2450
Wire Wire Line
	5450 2450 5550 2450
$Comp
L radiona4093-rescue:CONN_01X01 P1
U 1 1 5D57838A
P 5750 4100
F 0 "P1" H 5950 4100 50  0001 C CNN
F 1 "BAT-" H 5900 4100 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5750 4100 50  0001 C CNN
F 3 "" H 5750 4100 50  0000 C CNN
	1    5750 4100
	1    0    0    -1  
$EndComp
Connection ~ 5550 3850
Wire Wire Line
	5550 3700 5550 3850
$Comp
L radiona4093-rescue:CONN_01X01 P0
U 1 1 5D578390
P 5750 3850
F 0 "P0" H 5950 3850 50  0001 C CNN
F 1 "BAT+" H 5900 3850 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x01_Pitch2.54mm" H 5750 3850 50  0001 C CNN
F 3 "" H 5750 3850 50  0000 C CNN
	1    5750 3850
	1    0    0    -1  
$EndComp
$Comp
L radiona4093-rescue:GND #PWR01
U 1 1 58B43291
P 5550 4300
F 0 "#PWR01" H 5550 4050 50  0001 C CNN
F 1 "GND" H 5550 4150 50  0000 C CNN
F 2 "" H 5550 4300 50  0000 C CNN
F 3 "" H 5550 4300 50  0000 C CNN
	1    5550 4300
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 4100 5550 4300
Connection ~ 4900 3850
NoConn ~ 5550 2150
NoConn ~ 5550 2350
$EndSCHEMATC
